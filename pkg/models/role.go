package models

type UserRole struct {
	Id          int64 `gorm:"primaryKey"`
	Name        string
	Label       string
	Permissions []*Permission `gorm:"many2many:user_role_permissions"`
}

type Permission struct {
	Id     int64 `gorm:"primaryKey"`
	Method string
	Route  string
	Roles  []*UserRole `gorm:"many2many:user_role_permissions"`
}

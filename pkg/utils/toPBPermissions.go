package utils

import (
	"gitlab.com/delivery-go-react/back/role-svc/pkg/models"
	"gitlab.com/delivery-go-react/back/role-svc/pkg/pb"
)

func ToPBPermissions(permissions []*models.Permission) []*pb.Permission {
	var pbPermissions []*pb.Permission

	for _, p := range permissions {
		pbPermissions = append(pbPermissions, &pb.Permission{
			Id:     p.Id,
			Method: p.Method,
			Route:  p.Route,
		})
	}

	return pbPermissions
}

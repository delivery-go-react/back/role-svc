package db

import (
	"gitlab.com/delivery-go-react/back/role-svc/pkg/models"
	"gorm.io/gorm"
)

var defaultRoles = []models.UserRole{
	{
		Name:  "admin",
		Label: "Админ",
		Permissions: []*models.Permission{
			{Method: "*", Route: "*"},
		},
	},
	{
		Name:  "customer",
		Label: "Клиент",
		Permissions: []*models.Permission{
			{Method: "GET", Route: "/orders"},
			{Method: "POST", Route: "/orders"},
		},
	},
	{
		Name:  "courier",
		Label: "Курьер",
		Permissions: []*models.Permission{
			{Method: "GET", Route: "/role/get-permissions"},
		},
	},
	{
		Name:  "company",
		Label: "Компания",
		Permissions: []*models.Permission{
			{Method: "GET", Route: "/orders"},
			{Method: "PUT", Route: "/company"},
		},
	},
}

// Function to seed the default roles
func SeedDefaultRoles(db *gorm.DB) error {
	for _, role := range defaultRoles {
		var existingRole models.UserRole
		if err := db.Where(&models.UserRole{Name: role.Name}).First(&existingRole).Error; err != nil {
			if err == gorm.ErrRecordNotFound {
				if err := db.Create(&role).Error; err != nil {
					return err
				}
			} else {
				return err
			}
		}
	}
	return nil
}

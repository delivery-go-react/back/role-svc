package services

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/delivery-go-react/back/role-svc/pkg/db"
	"gitlab.com/delivery-go-react/back/role-svc/pkg/models"
	"gitlab.com/delivery-go-react/back/role-svc/pkg/pb"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Server struct {
	H db.Handler
}

func (s *Server) GetAllRoles(ctx context.Context, req *pb.EmptyRequest) (*pb.GetAllRolesResponse, error) {
	var roles []*models.UserRole

	if result := s.H.DB.Find(&roles); result.Error != nil {
		return &pb.GetAllRolesResponse{
			Status: int64(http.StatusNotFound),
		}, status.Errorf(codes.NotFound, "Error retrieving roles: %v", result.Error)
	}

	res := &pb.GetAllRolesResponse{
		Status: int64(http.StatusOK),
		Roles:  make([]*pb.Role, 0, len(roles)),
	}

	for _, r := range roles {
		res.Roles = append(res.Roles, &pb.Role{
			Id:    r.Id,
			Name:  r.Name,
			Label: r.Label,
			// Permissions: utils.ToPBPermissions(r.Permissions),
		})
	}

	return res, nil
}

func (s *Server) GetAllPermissions(ctx context.Context, req *pb.EmptyRequest) (*pb.GetAllPermissionsResponse, error) {
	var permissions []*models.Permission

	if result := s.H.DB.Find(&permissions); result.Error != nil {
		return &pb.GetAllPermissionsResponse{
			Status: int64(http.StatusNotFound),
		}, status.Errorf(codes.NotFound, "Error retrieving permissions: %v", result.Error)
	}

	res := &pb.GetAllPermissionsResponse{
		Status:      int64(http.StatusOK),
		Permissions: make([]*pb.Permission, 0, len(permissions)),
	}

	for _, p := range permissions {
		res.Permissions = append(res.Permissions, &pb.Permission{
			Id:     p.Id,
			Method: p.Method,
			Route:  p.Route,
		})
	}

	return res, nil
}

func (s *Server) CheckPermission(ctx context.Context, req *pb.CheckPermissionRequest) (*pb.Response, error) {
	method := req.Method
	route := req.Route
	fmt.Println("CheckPermission", req)

	var role models.UserRole
	if result := s.H.DB.Preload("Permissions").Where(&models.UserRole{Id: req.RoleId}).First(&role); result.Error != nil {
		return &pb.Response{
			Status: http.StatusNotFound,
			Error:  "Role not found",
		}, nil
	}
	fmt.Println("role", role)

	for _, permission := range role.Permissions {
		if (permission.Method == method && permission.Route == route) || (permission.Method == "*" && permission.Route == "*") {
			return &pb.Response{
				Status: http.StatusOK,
			}, nil
		}
	}

	return &pb.Response{
		Status: http.StatusUnauthorized,
		Error:  "User does not have permission to access this resource",
	}, nil
}

func (s *Server) CreateRole(ctx context.Context, req *pb.CreateRoleRequest) (*pb.Response, error) {
	return &pb.Response{
		Status: http.StatusUnauthorized,
		Error:  "Temporary do not work",
	}, nil
}

func (s *Server) UpdateRole(ctx context.Context, req *pb.UpdateRoleRequest) (*pb.Response, error) {
	return &pb.Response{
		Status: http.StatusUnauthorized,
		Error:  "Temporary do not work",
	}, nil
}

func (s *Server) CreatePermission(ctx context.Context, req *pb.CreatePermissionRequest) (*pb.Response, error) {
	return &pb.Response{
		Status: http.StatusUnauthorized,
		Error:  "Temporary do not work",
	}, nil
}

func (s *Server) UpdatePermission(ctx context.Context, req *pb.UpdatePermissionRequest) (*pb.Response, error) {
	return &pb.Response{
		Status: http.StatusUnauthorized,
		Error:  "Temporary do not work",
	}, nil
}
